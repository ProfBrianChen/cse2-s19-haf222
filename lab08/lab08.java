//Haoluo Fu
//04/08/2019
//This is for the lab 08

import java.util.Arrays;
public class lab08{
  
  public static int getRange(int[] list){
    
    Arrays.sort(list);
    System.out.print("The sorted array is ");
    for(int i = 0; i < list.length; i++){
      
      System.out.print(list[i]+" ");
      
    }
    System.out.println("");
    int ab = list.length - 1;
    int range = list[ab] - list[0];
    System.out.println("The range is "+range);
    return range;
    
  }
  
  public static double getMean(int[] list){
    
    double ab = 0;
    for(int i = 0; i < list.length; i++){
      
      ab = ab + list[i];
      
    }
    ab /= (list.length);
    System.out.println("The mean is "+ab);
    return ab;
    
  }
  
  public static double getStdDev(int[] list, double ab){
    double wresult = 0;
    double j = list.length;
    for(int i = 0; i < list.length; i++){
      
      wresult += (list[i] - ab) * (list[i] - ab) / (j - 1);
      
    }
    wresult = Math.sqrt(wresult);
    System.out.println("The standard deviation is "+wresult);
    return wresult;
    
  }
  
  public static void shuffle(int[] list){
    int ab;
    int cd;
    int trans = 0;
    for(int i = 0; i < list.length; i++){
      
      ab = (int)(Math.random() * list.length);
      cd = (int)(Math.random() * list.length);
      trans = list[ab];
      list[ab] = list[cd];
      list[cd] = trans;
      
    }
    System.out.print("The shuffled array is ");
    for(int z = 0; z < list.length; z++){
      
      System.out.print(list[z]+" ");
      
    }
    System.out.println("");
  }
  
  public static void main(String args[]){
    
    System.out.print("The initial array is ");
    int rnum = (int)(Math.random() * 51 + 50);
    int[] array = new int [rnum];
    for(int i = 0; i < rnum; i++){
      
      array[i] = (int)(Math.random() * 100);
      System.out.print(array[i]);
    }
    System.out.println("");
    int range = getRange(array);
    Arrays.sort(array);
    double mean = getMean(array);
    double std = getStdDev(array, mean);
    shuffle(array);
    
  }
  
}