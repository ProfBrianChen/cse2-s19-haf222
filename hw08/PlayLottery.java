//Haoluo Fu
//04/11/2019
//PlayLottery program

import java.util.Scanner;
public class PlayLottery{
  
  //returns true when user and winning are the same. 
  public static boolean userWins(int[] user, int[] winning){
    
    //the initiatives
    boolean result;
    result = false;
    boolean wa;
    boolean ha;
    
    //the loop for comparing numbers
    for(int i = 0; i < 4; i++){
      if(user[i] == user[i+1] && winning[i] == winning[i+1]){
        result = true;
        continue;
      }
      wa = user[i] < user[i+1];
      ha = winning[i] < winning[i+1];
      if(wa != ha){
        result = false;
        break;
      }
      result = true;
    }
    return result;
  }

  //generates the random numbers for the lottery without duplication.
  public static int[] numbersPicked(){
    System.out.print("The winning numbers are: ");
    int[] array = new int [5];
    
    //the loop that creates random number
    for(int i = 0; i < 5; i++){
      
      array[i] = (int)(Math.random() * 59);
      System.out.print(array[i]);
      if(i == 4){
        break;
      }
      System.out.print(", ");
    }
    System.out.println("");
    return array;
    
  }

  
  public static void main(String args[]){
    
    //the initiaves
    Scanner scan;
    scan = new Scanner(System.in);
    int[] user = new int [5];
    int[] computer = new int [5];
    System.out.print("Enter 5 numbers between 0 and 59: ");
    user[0] = scan.nextInt();
    
    //the loop for generating marks 
    for(int z = 1; z < 5; z++){
      
      System.out.print(", ");
      user[z] = scan.nextInt();
      
    }
    System.out.println("");
    computer = numbersPicked();
    boolean answer = userWins(user, computer);
    
    // the final result
    if(answer == true){
      System.out.println("You win!");
    }else{
      System.out.println("You lose");
    }
    
  }
  
}