//Haoluo Fu
//04/09/2019
//this is for the first program

public class Letters{
  
  //the first method to sort out characters from A to M
  public static void getAtoM(char[] list1){
    System.out.print("AtoM characters: ");
    for(int i = 0; i < list1.length; i++){
      if(list1[i] <= 'M' && list1[i] >= 'A' || list1[i] <= 'm' && list1[i] >= 'a'){
        System.out.print(list1[i]);
      }
    }
  }
  
  //the second method to sort out characters from N to Z
  public static void getNtoZ(char[] list2){
    System.out.print("NtoZ characters: ");
    for(int i = 0; i < list2.length; i++){
      if(list2[i] <= 'M' && list2[i] >= 'A' || list2[i] <= 'm' && list2[i] >= 'a'){
        
      }else{
        System.out.print(list2[i]);
      }
    }
  }
  
  public static void main(String args[]){
    
    //the initial setting for the abcd characters
    String alist = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
    //the random for how many space in a array
    int space = (int)(Math.random() * 30);
    char[] array = new char [space];
    int spe1;
    System.out.print("Random character array: ");
    
    //the random array
    for(int i = 0; i < space; i++){
      spe1 = (int)(Math.random() * 48);
      array[i] = alist.charAt(spe1);
        System.out.print(array[i]);
    }
    System.out.println("");
    getAtoM(array);
    System.out.println("");
    getNtoZ(array);
    System.out.println("");
    
  }
  
}