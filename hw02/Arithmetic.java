//Haoluo Fu
//02/08/2019
//This is a program for calculating how much you pay for your shopping

public class Arithmetic{
  
  public static void main(String args[]){
    
    //number of pairs of pants
    int numPants = 3;
    //cost per pair of pants
    double pantsPrice = 34.98;
    
    //number of sweat shirts
    int numShirts = 2;
    //cost per shirts
    double shirtPrice = 24.99;
    
    //number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //Total cost of each kind of item (i.e. total cost of pants, etc)
    double totalCostPants = numPants * pantsPrice;
    double totalCostShirts = numShirts * shirtPrice;
    double totalCostBelt = numBelts * beltCost;
    
    //Sales tax charged buying all of each kind of item i.e. sales tax charged on belts
    double salesTaxPants = totalCostPants * paSalesTax;
    double salesTaxShirts = totalCostShirts * paSalesTax;
    double salesTaxBelt = totalCostBelt * paSalesTax;
    
    //Total cost of purchases before tax
    double totalCostPurchase = totalCostPants + totalCostShirts + totalCostBelt;
    
    //Total sales tax
    double totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelt;
    
    //Total paid for this transaction, including sales tax
    double totalPaid = totalCostPurchase + totalSalesTax;
    
    //elimilate decimals
    double totalCostPurchase2 = (int) (totalCostPurchase * 100) / 100.0;
    double totalSalesTax2 = (int) (totalSalesTax * 100) / 100.0;
    double totalPaid2 = (int) (totalPaid * 100) / 100.0;
    
    //display the total cost of the purchases (before tax), the total sales tax, and the total cost of the purchases including sales tax
    System.out.println("The total cost of purchases before tax is " + totalCostPurchase2);
    System.out.println("The total sales tax is " + totalSalesTax2);
    System.out.println("The total cost of the purchases including sales tax is " + totalPaid2);
    
  }
  
}