///Haoluo Fu
//Jan 27 2019
///
public class WelcomeClass{
  
  public static void main (String args[]){
    
    ///print a screen that displays welcome
    
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-H--A--F--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v ");
    
  }
  
}