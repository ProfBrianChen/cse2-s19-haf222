//Haoluo Fu
//03/22/2019
//this is for lab07

import java.util.Random;
import java.util.Scanner;

public class lab07{

	public static String ad(){

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		String adj = "error";
		switch(randomInt){
		case 1:
			adj = "great";
			break;
		case 2:
			adj = "fantastic";
			break;
		case 3:
			adj = "friendly";
			break;
		case 4:
			adj = "crazy";
			break;
		case 5:
			adj = "amazing";
			break;
		case 6:
			adj = "happy";
			break;
		case 7:
			adj = "super";
			break;
		case 8:
			adj = "smart";
			break;
		case 9:
			adj = "sad";
			break;
		case 0:
			adj = "rude";
		}
		return adj;

	}

	public static String nouns(){

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		String adj = "error";
		switch(randomInt){
		case 1:
			adj = "fox";
			break;
		case 2:
			adj = "spider-man";
			break;
		case 3:
			adj = "policeman";
			break;
		case 4:
			adj = "rabbit";
			break;
		case 5:
			adj = "samurai";
			break;
		case 6:
			adj = "knight";
			break;
		case 7:
			adj = "king";
			break;
		case 8:
			adj = "queen";
			break;
		case 9:
			adj = "tiger";
			break;
		case 0:
			adj = "superman";
			break;
		}
		return adj;

	}

	public static String pVerbs(){

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		String adj = "error";
		switch(randomInt){
		case 1:
			adj = "helped";
			break;
		case 2:
			adj = "fixed";
			break;
		case 3:
			adj = "saved";
			break;
		case 4:
			adj = "destroyed";
			break;
		case 5:
			adj = "hit";
			break;
		case 6:
			adj = "ate";
			break;
		case 7:
			adj = "kissed";
			break;
		case 8:
			adj = "touched";
			break;
		case 9:
			adj = "embraced";
			break;
		case 0:
			adj = "kicked";
		}
		return adj;

	}

	public static String pNouns(){

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		String adj = "error";
		switch(randomInt){
		case 1:
			adj = "computers";
			break;
		case 2:
			adj = "smartphones";
			break;
		case 3:
			adj = "keyboards";
			break;
		case 4:
			adj = "televisions";
			break;
		case 5:
			adj = "hearts";
			break;
		case 6:
			adj = "enemies";
			break;
		case 7:
			adj = "hands";
			break;
		case 8:
			adj = "dinosaurs";
			break;
		case 9:
			adj = "shield";
			break;
		case 0:
			adj = "tissues";
			break;
		}
		return adj;

	}

	public static String sentence(){

		String sugoi;
		String adj1;
		String adj2;
		String adj3;
		String noun;
		String pv;
		String pn;
		adj1 = ad();
		adj2 = ad();
		adj3 = ad();
		noun = nouns();
		pv = pVerbs();
		pn = pNouns();
		sugoi = "The "+adj1+" "+adj2+" "+noun+" "+pv+" the "+adj3+" "+pn+".";
		return sugoi;

	}

	public static void sentence2(){

		String sugoi;
		String adj1;
		String n3;
		String noun;
		String pv;
		String pn;
		adj1 = ad();
		n3 = pNouns();
		noun = pNouns();
		pv = pVerbs();
		pn = pNouns();
		System.out.println("It used "+n3+" to "+pv+" "+noun+" "+pv+" at the "+adj1+" "+pn+".");
		
	}

	public static String conclusion(){

		String sugoi;
		String pv;
		String noun;
		String n2;
		pv = pVerbs();
		noun = nouns();
		n2 = pNouns();
		sugoi = "That "+noun+" "+pv+" her "+n2+"!";
		return sugoi;

	}

	public static String finalS(){

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		String s1 = sentence();
		String s2;
		String end = conclusion();
		System.out.println(s1);
		while(randomInt > 0){
			sentence2();
			randomInt--;
		}
		System.out.println(end);
		return s1;
	}

	public static void main(String args[]){

		Scanner myScanner;
		myScanner = new Scanner(System.in);
		int sta = 1;
		String sen;
		String para;
		do{
			sen = sentence();
			System.out.println(sen);
			System.out.print("Would you like to print out another sentence? Press 1 to continue or press 0 to stop ");
			sta = myScanner.nextInt();
		}while(sta > 0);
		para = finalS();
	}

}
