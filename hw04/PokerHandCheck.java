//Haoluo Fu
//02/18/2019
//A simple poker hand interpreter

public class PokerHandCheck{
  
  public static void main(String args[]){
    
    //draw the first card
     int card = (int)(Math.random()*52)+1;
      if (card > 0 && card <= 13){
      
      switch (card){
          
        case 1:
          System.out.println("You picked the ace of Clubs");
          break;
        case 11:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 12:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 13:
          System.out.println("You picked the King of Clubs");
          break;
        default:
          System.out.println("You picked the "+card+" of Clubs");
          break;
      }  
      
      }else if(card > 13 && card <= 26){
        switch (card){
            
        case 14:
          System.out.println("You picked the ace of Diamonds");
        break;
        case 24:
          System.out.println("You picked the Jack of Diamonds");
        break;
        case 25:
          System.out.println("You picked the Queen of Diamonds");
        break;
        case 26:
          System.out.println("You picked the King of Diamonds");
        break;
        default:
        card = card%13;
        System.out.println("You picked the "+card+" of Diamonds");
        break;
        }
      }else if(card > 26 && card <=39){
        switch(card){
        case 27:
          System.out.println("You picked the ace of Hearts");
        break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 39:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card = card%13;
        System.out.println("You picked the "+card+" of Hearts");
        break;
        }
      }else if(card > 39 && card <= 52){
        switch(card){
        case 40:
          System.out.println("You picked the ace of Hearts");
        break;
        case 50:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 51:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 52:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card = card%13;
        System.out.println("You picked the "+card+" of Hearts");
        break;
        }
      }
    card = card%13;
    
    //draw the second card
     int card2 = (int)(Math.random()*52)+1;
      if (card2 > 0 && card2 <= 13){
      
      switch (card2){
          
        case 1:
          System.out.println("You picked the ace of Clubs");
          break;
        case 11:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 12:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 13:
          System.out.println("You picked the King of Clubs");
          break;
        default:
          System.out.println("You picked the "+card2+" of Clubs");
          break;
      }  
      
      }else if(card2 > 13 && card2 <= 26){
        switch (card2){
            
        case 14:
          System.out.println("You picked the ace of Diamonds");
        break;
        case 24:
          System.out.println("You picked the Jack of Diamonds");
        break;
        case 25:
          System.out.println("You picked the Queen of Diamonds");
        break;
        case 26:
          System.out.println("You picked the King of Diamonds");
        break;
        default:
        card2 = card2%13;
        System.out.println("You picked the "+card2+" of Diamonds");
        break;
        }
      }else if(card2 > 26 && card2 <=39){
        switch(card2){
        case 27:
          System.out.println("You picked the ace of Hearts");
        break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 39:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card2 = card2%13;
        System.out.println("You picked the "+card2+" of Hearts");
        break;
        }
      }else if(card2 > 39 && card2 <= 52){
        switch(card2){
        case 40:
          System.out.println("You picked the ace of Hearts");
        break;
        case 50:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 51:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 52:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card2 = card2%13;
        System.out.println("You picked the "+card2+" of Hearts");
        break;
        }
      }
    card2 = card2%13;
    
    //draw the third card
     int card3 = (int)(Math.random()*52)+1;
      if (card3 > 0 && card3 <= 13){
      
      switch (card3){
          
        case 1:
          System.out.println("You picked the ace of Clubs");
          break;
        case 11:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 12:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 13:
          System.out.println("You picked the King of Clubs");
          break;
        default:
          System.out.println("You picked the "+card3+" of Clubs");
          break;
      }  
      
      }else if(card3 > 13 && card3 <= 26){
        switch (card3){
            
        case 14:
          System.out.println("You picked the ace of Diamonds");
        break;
        case 24:
          System.out.println("You picked the Jack of Diamonds");
        break;
        case 25:
          System.out.println("You picked the Queen of Diamonds");
        break;
        case 26:
          System.out.println("You picked the King of Diamonds");
        break;
        default:
        card3 = card3%13;
        System.out.println("You picked the "+card3+" of Diamonds");
        break;
        }
      }else if(card3 > 26 && card3 <=39){
        switch(card3){
        case 27:
          System.out.println("You picked the ace of Hearts");
        break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 39:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card3 = card3%13;
        System.out.println("You picked the "+card3+" of Hearts");
        break;
        }
      }else if(card3 > 39 && card3 <= 52){
        switch(card3){
        case 40:
          System.out.println("You picked the ace of Hearts");
        break;
        case 50:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 51:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 52:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card3 = card3%13;
        System.out.println("You picked the "+card3+" of Hearts");
        break;
        }
      }
    card3 = card3%13;
    
    //draw the fourth card
     int card4 = (int)(Math.random()*52)+1;
      if (card4 > 0 && card4 <= 13){
      
      switch (card4){
          
        case 1:
          System.out.println("You picked the ace of Clubs");
          break;
        case 11:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 12:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 13:
          System.out.println("You picked the King of Clubs");
          break;
        default:
          System.out.println("You picked the "+card4+" of Clubs");
          break;
      }  
      
      }else if(card4 > 13 && card4 <= 26){
        switch (card4){
            
        case 14:
          System.out.println("You picked the ace of Diamonds");
        break;
        case 24:
          System.out.println("You picked the Jack of Diamonds");
        break;
        case 25:
          System.out.println("You picked the Queen of Diamonds");
        break;
        case 26:
          System.out.println("You picked the King of Diamonds");
        break;
        default:
        card4 = card4%13;
        System.out.println("You picked the "+card4+" of Diamonds");
        break;
        }
      }else if(card4 > 26 && card4 <=39){
        switch(card4){
        case 27:
          System.out.println("You picked the ace of Hearts");
        break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 39:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card4 = card4%13;
        System.out.println("You picked the "+card4+" of Hearts");
        break;
        }
      }else if(card4 > 39 && card4 <= 52){
        switch(card4){
        case 40:
          System.out.println("You picked the ace of Hearts");
        break;
        case 50:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 51:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 52:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card4 = card4%13;
        System.out.println("You picked the "+card4+" of Hearts");
        break;
        }
      }
    card4 = card4%13;
    
    //draw the fifth card
     int card5 = (int)(Math.random()*52)+1;
      if (card5 > 0 && card5 <= 13){
      
      switch (card5){
          
        case 1:
          System.out.println("You picked the ace of Clubs");
          break;
        case 11:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 12:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 13:
          System.out.println("You picked the King of Clubs");
          break;
        default:
          System.out.println("You picked the "+card5+" of Clubs");
          break;
      }  
      
      }else if(card5 > 13 && card5 <= 26){
        switch (card5){
            
        case 14:
          System.out.println("You picked the ace of Diamonds");
        break;
        case 24:
          System.out.println("You picked the Jack of Diamonds");
        break;
        case 25:
          System.out.println("You picked the Queen of Diamonds");
        break;
        case 26:
          System.out.println("You picked the King of Diamonds");
        break;
        default:
        card5 = card5%13;
        System.out.println("You picked the "+card5+" of Diamonds");
        break;
        }
      }else if(card5 > 26 && card5 <=39){
        switch(card5){
        case 27:
          System.out.println("You picked the ace of Hearts");
        break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 39:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card5 = card5%13;
        System.out.println("You picked the "+card5+" of Hearts");
        break;
        }
      }else if(card5 > 39 && card5 <= 52){
        switch(card5){
        case 40:
          System.out.println("You picked the ace of Hearts");
        break;
        case 50:
          System.out.println("You picked the Jack of Hearts");
        break;
        case 51:
          System.out.println("You picked the Queen of Hearts");
        break;
        case 52:
          System.out.println("You picked the King of Hearts");
        break;
        default:
        card5 = card5%13;
        System.out.println("You picked the "+card5+" of Hearts");
        break;
        }
      }
    card5 = card5%13;
    
    //give the conditoin of 2 cards equal
    boolean ab = card==card2;
    boolean ac = card==card3;
    boolean ad = card==card4;
    boolean ae = card==card5;
    
    boolean bc = card2==card3;
    boolean bd = card2==card4;
    boolean be = card2==card5;
    
    boolean cd = card3==card4;
    boolean ce = card3==card5;
    
    boolean de = card4==card5;
    
    //give the condition of 3 cards equal
    boolean abc = ab&&bc;
    boolean abd = ab&&bd;
    boolean abe = ab&&be;
    
    boolean acd = ac&&cd;
    boolean ace = ac&&ce;
    
    boolean ade = ad&&de;
    
    //give the condition of 2 pairs
    boolean abcd = ab&&cd;
    boolean abce = ab&&ce;
    boolean abde = ab&&de;
    
    boolean acbd = ac&&bd;
    boolean acbe = ac&&be;
    boolean acde = ac&&de;
    
    boolean adbc = ad&&bc;
    boolean adbe = ad&&be;
    boolean adce = ad&&ce;
    
    boolean aebc = ae&&bc;
    boolean aebd = ae&&bd;
    boolean aecd = ae&&cd;
    
    //give a blank line
    System.out.println("      ");
    
    //print out when three of a kind is detected
    if(abc||abd||abe||acd||ace||ade){
      
      System.out.println("You have three of a kind!");
        
    }else if(abcd||abce||abde||acbd||acbe||acde||adbc||adbe||adce||aebc||aebd||aecd){
      
      // print out when 2 pairs is detected
      System.out.println("You have two pairs!");
      
    }else if(ab||ac||ad||ae||bc||bd||be||cd||ce||de){
      
      // print out when a pair is detected
      System.out.println("You have a pair!");
      
    }else{
      
      //print out when you have a high card hand
      System.out.println("You have a high card hand!");
      
    }
      
  }
  
}