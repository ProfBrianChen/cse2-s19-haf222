//Haoluo Fu
//03/08/2019
//PatternC

import java.util.Scanner;

public class PatternC{
  
  public static void main(String args[]){
    
    Scanner kasumi;
    kasumi = new Scanner(System.in);
    System.out.print("Enter an integer between 1 - 10 ");
    
    //the initial values
    int checker = 0;
    int k = 0;
    
    //the loop for check
    while(checker == 0){
      
      boolean o = kasumi.hasNextInt();
      //check if it is an integer
      if(o == true){
        k = kasumi.nextInt();
        //check is the integer is between 1 - 10
        if(k >= 1 && k <= 10){
          
          break;
          
        }
        System.out.print("An error occured! Please Enter an integer between 1 - 10 ");
        continue;
      }
      String blabla = kasumi.next();
      System.out.print("An error occured! Please Enter an integer between 1 - 10 ");
    }
    
    //the outerloop for printing 
    for(int z = 1; z <= k; z++){
      
      //the innerloop for printing space
      for(int s = (10 - z); s >= 1; s--){
        
        System.out.print(" ");
        
      }
      
      //the inner loop for printing numbers
      for(int j = z; j >= 1; j--){
        
        System.out.print(j);
        
      }
      System.out.println();
    }
    
  }
  
}