//Haoluo Fu
//04/19/2019
//Searching

import java.util.Scanner;
public class Searching{
  
  //the method for generating array
  public static int[] generate(int ab){
    
    int[] aven = new int [ab];
    for(int z = 0; z < aven.length; z++){
      
      aven[z] = (int)(Math.random() * aven.length);
      
    }
    return aven;
    
  }
  
  //the method for generating array
  public static int[] random1(int ab){
    
    int[] aven = new int [ab];
    aven[0] = 0;
    for(int z = 1; z < aven.length; z++){
      
      aven[z] = (int)(Math.random() * 3) + aven[z - 1] + 1;
      
    }
    return aven;
    
  }
  
  public static int linsear(int[] haha, int ab){
    
    int ok = -1;
    for(int z = 0; z < haha.length; z++){
      
      if(haha[z] == ab){
        
        ok = z;
        break;
        
      }
      
    }
    return ok;
  }
  
    // Returns index of x if it is present in arr[l.. 
    // r], else return -1 
    public static int binsear(int arr[], int l, int r, int x){ 
        if (r >= l) { 
            int mid = l + (r - l) / 2; 
  
            // If the element is present at the 
            // middle itself 
            if (arr[mid] == x) 
                return mid; 
  
            // If element is smaller than mid, then 
            // it can only be present in left subarray 
            if (arr[mid] > x) 
                return binsear(arr, l, mid - 1, x); 
  
            // Else the element can only be present 
            // in right subarray 
            return binsear(arr, mid + 1, r, x); 
        } 
  
        // We reach here when element is not present 
        // in array 
        return -1; 
    } 
  
  //the method for printing array
  public static void print(int[] well){
    
    for(int i = 0; i < well.length; i++){
      
      System.out.print(well[i]+","+" ");
      
    }
    System.out.println("");
    
  }
  
  public static void main(String args[]){
    
    Scanner scan;
    scan = new Scanner(System.in);
    System.out.print("Do you want to do a binary search or linear search? Press 2 for binary search and press 1 to do linear search ");
    int data = scan.nextInt();
    System.out.print("enter the array size ");
    int size = scan.nextInt();
    System.out.print("Enter the index ");
    int index = scan.nextInt();
    int[] array1;
    int result;
    if(data == 1){
      
      array1 = generate(size);
      print(array1);
      result = linsear(array1, index);
      System.out.println("index is "+result);
      
    }
    
    if(data == 2){
      
      array1 = random1(size);
      print(array1);
      result = binsear(array1, 0, size, index);
      System.out.println("index is "+result);
    }
    
  }
  
}