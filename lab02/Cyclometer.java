///Haoluo Fu
//02/01/2019
///
public class Cyclometer{
  
  public static void main (String args[]){
    
    //our input data
    int secsTrip1=480;  //The seconds elapsed during the 1st trip
    int secsTrip2=3220;  //The seconds elapsed during the 2nd trip
    int countsTrip1=1561;  //The number of rotation of 1st trip
    int countsTrip2=9037;  //The number of rotation of 2nd trip
    
    //our intermediate variables and output data
    double wheelDiameter=27.0, //the diameter of the wheel is in inches 
    PI=3.14159,  //the pi we use in out daily math
    feetPerMile=5280,  //every mile equals 5280 feet
    inchesPerFoot=12,  //every foot equals 12 inches
    secondsPerMinute=60;  //every minute equals 60 seconds
    double distanceTrip1, distanceTrip2, totalDistance;  //set the types of three variables to double type
    
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    
    //run the calculations; store the values.
    distanceTrip1=countsTrip1*wheelDiameter*PI; 
    // Above gives distance in inches (for each count, a rotation of the wheel travels the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; //This gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/(inchesPerFoot*feetPerMile); 
    totalDistance=distanceTrip1+distanceTrip2;
    
    //print out the output data
    System.out.println("Trip1 was "+distanceTrip1+" miles.");
    System.out.println("Trip2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance was "+totalDistance+" miles.");
    
  }
  
}