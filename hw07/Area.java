//Haoluo Fu
//03/25/2019

//import of java
import java.util.Scanner;

public class Area{
 
  //the rectangle
  public static double recta(){
    double a;
    double b;
    boolean check;
    Scanner xScan;
    xScan = new Scanner(System.in);
    
    //get the base from user
    System.out.print("Enter the value of the base of rectangle ");
    while(1 != 0){
      check = xScan.hasNextDouble();
      if(check == true){
        a = xScan.nextDouble();
        if(a >= 0){
          break;
        }
        System.out.print("Error! Please input the value bigger or equal to zero in double ");
        continue;
      }
      String whatever = xScan.next();
      System.out.print("Error! Please input the value bigger or equal to zero in double ");
    }
    
    //get the height from user
    System.out.print("Enter the value of height of rectangle ");
    while(1 != 0){
      check = xScan.hasNextDouble();
      if(check == true){
        b = xScan.nextDouble();
        if(b >= 0){
          break;
        }
        System.out.print("Error! Please input the value bigger or equal to zero in double ");
        continue;
      }
      String whatever = xScan.next();
      System.out.print("Error! Please input the value bigger or equal to zero in double ");
    }
    
    //calculation
    double rectArea = a * b;
    
    return rectArea;
  }
  
  //the triangle
  public static double trian(){
    double a;
    double b;
    boolean check;
    Scanner xScan;
    xScan = new Scanner(System.in);
    
    //get the base from user
    System.out.print("Enter the value of the base of triangle ");
    while(1 != 0){
      check = xScan.hasNextDouble();
      if(check == true){
        a = xScan.nextDouble();
        if(a >= 0){
          break;
        }
        System.out.print("Error! Please input the value bigger or equal to zero in double ");
        continue;
      }
      String whatever = xScan.next();
      System.out.print("Error! Please input the value bigger or equal to zero in double ");
    }
    
    //get the height from user
    System.out.print("Enter the value of height of triangle ");
    while(1 != 0){
      check = xScan.hasNextDouble();
      if(check == true){
        b = xScan.nextDouble();
        if(b >= 0){
          break;
        }
        System.out.print("Error! Please input the value bigger or equal to zero in double ");
        continue;
      }
      String whatever = xScan.next();
      System.out.print("Error! Please input the value bigger or equal to zero in double ");
    }
    
    //calculation
    double triArea = a * b;
    
    return triArea;
  }
  
  //the circle
  public static double cir(){
    double a;
    boolean check;
    Scanner xScan;
    xScan = new Scanner(System.in);
    
    //get the radius from user
    System.out.print("Enter the value of the radius of the circle ");
    while(1 != 0){
      check = xScan.hasNextDouble();
      if(check == true){
        a = xScan.nextDouble();
        if(a >= 0){
          break;
        }
        System.out.print("Error! Please input the value bigger or equal to zero in double ");
        continue;
      }
      String whatever = xScan.next();
      System.out.print("Error! Please input the value bigger or equal to zero in double ");
    }
    
    //calculation
    double cirArea = Math.PI * a * a;
    
    return cirArea;
  }
  
  //the real checker
  public static double checker(){
    double area;
    Scanner xScan;
    xScan = new Scanner(System.in);
    System.out.print("Enter the shape you want to calculate(rectangle ,triangle, circle) ");
    while(1 != 0){
      String shape = xScan.next();
      if(shape.equals("rectangle")){
        area = recta();
        break;
      }else if(shape.equals("triangle")){
        area = trian();
        break;
      }else if(shape.equals("circle")){
        area = cir();
        break;
      }
      System.out.print("Error! invalid shapes! Please enter a shape from rectangle, triangle, and circle ");
    }
    
    return area;
  }
  
  public static void main(String args[]){
    
    double ares = checker();
    System.out.println("The area is "+ares);
    
  }
  
}