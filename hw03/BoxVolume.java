//Haoluo Fu
//02/08/2019
//This program is for calculating the volume of a box

import java.util.Scanner;

public class BoxVolume{
  
  public static void main(String args[]){
    
    //get the user inputs
    Scanner myScanner = new Scanner (System.in);
    System.out.print("The width side of the box is: ");
    double width = myScanner.nextDouble();
    System.out.print("The length of the box is: ");
    double length = myScanner.nextDouble();
    System.out.print("The height of the box is: ");
    double height = myScanner.nextDouble();
    
    //do the calculation of the volume
    double volume = width * length * height;
    System.out.println("   ");
    System.out.println("The volume inside the box is: " + volume);
    
  }
  
}