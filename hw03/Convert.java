//Haoluo Fu
//02/08/2019
//This program is for users to convert meters into inches

//get the user inputs
import java.util.Scanner;

public class Convert{
  
  public static void main(String args[]){
    
    //get the user inputs 
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter the distance in meters: ");
    double meters = myScanner.nextDouble();
    
    //convert meters into inches 
    double inches = (int) (meters * 39.3700787 * 10000) / 10000.0;
    System.out.println(meters + " meters is " + inches + " inches");
    
  }
  
}