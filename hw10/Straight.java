//Haoluo Fu
//04/28/2019
//for getting a straight

public class Straight{
  
  //the method to generate a card from 52 cards
  public static int generator(){
    
    int[] card = new int [52];
    int target;
    int temp;
    for(int i = 0; i < 52; i++){
      
      card[i] = i;
      
    }
    for (int i = 0; i < 52; i++){
      
	    //find a random member to swap with
	    target = (int) (card.length * Math.random() );
      
      //swap the values
	    temp = card[target];
	    card[target] = card[i];
	    card[i] = temp;
      
    }
    int tcard = card[(int) (card.length * Math.random() )];
    return tcard;
    
  }
  
  //the method to draw 5 cards out of 52 cards
  public static int[] drawer(){
    
    int[] cards = new int [5];
    for(int i = 0; i < 5; i++){
      
      cards[i] = generator();
      
    }
    return cards;
    
  }
  
  //the method to do the precise search of the lowest number
  public static int searcher(int[] hand, int k){
    
    if(k > 5 || k <= 0){
      
      System.out.println("Error!!! The value returned is -1");
      return -1;
      
    }
    int st;
    int nd;
    int temp;
    
    //for assorting the array and do the linear search
    for(int i = 0; i < 5; i++){
      
      
      for(int j = i + 1; j < 5; j++){
        
        st = (hand[i] + 1) % 13;
        nd = (hand[j] + 1) % 13;
        if(st > nd){
          
          temp = hand[i];
          hand[i] = hand[j];
          hand[j] = temp;
          
        }
        
      }
      
      if(i == k - 1){
        
        temp = hand[k - 1];
        return temp;
        
      }
      
    }
    return -100;
    
  }
  
  //the method for evaluating the appearance of the straight
  public static boolean straight(int[] hand){
    
    for(int i = 1; i < 5; i++){
      
      if(((searcher(hand, i) + 1) % 13) + 1 != ((searcher(hand, (i+1)) + 1) % 13)){
        
        return false;
        
      }
      
    }
    return true;
    
  }
  
  public static void main(String args[]){
    
    int[] card = new int [5];
    boolean result;
    int counter = 0;
    
    //the loop that ran for a million times
    for(int i = 0; i < 1000000; i++){
      
      card = drawer();
      result = straight(card);
      if(result == true){
        
        counter++;
        
      }
      
    }
    System.out.println("The probability is 0." + counter + "%");
  }
  
}