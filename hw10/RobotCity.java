//Haoluo Fu
//04/28/2019
//The program for the robot city

public class RobotCity{

  //the method to build the array city
	public static int[][] buildCity(){

		//to define the number of spaces in 2 dimensional arrays
    int[][] city = new int [(int)(Math.random() * 6 + 10)] [(int)(Math.random() * 6 + 10)];
    
    //for the 1st dimension
		for(int i = 0; i < city.length; i++){

			//for the 2nd dimension
      for(int j = 0; j < city[i].length; j++){

				city[i][j] = (int)(Math.random() * 900 + 100);

			}

		}
		return city;

	}

	//the method to print out the 2 dimensional arrays
  public static void display(int[][] city){

		for(int i = 0; i < city.length; i++){

			for(int j = 0; j < city[i].length; j++){

				System.out.printf("%4d  ", city[i][j]);

			}
			System.out.println("");
			System.out.println("");

		}
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
	}

	//the invasion method 
  public static void invade(int[][] city, int k){
		int x;
		int y;
		int hana;
		for(int i = 0; i < k; i++){
			x = (int)(Math.random() * city.length);
			y = (int)(Math.random() * city[0].length);
			hana = city[x][y];
			while(hana < 0){

				x = (int)(Math.random() * city.length);
				y = (int)(Math.random() * city[0].length);
				hana = city[x][y];

			}
			city[x][y] = 0 - hana;
		}

	}

	//the method to change the position of the robots
  public static void update(int[][] city){

		for(int i = 0; i < city.length; i++){

			for(int j = city[i].length - 1; j >= 0; j--){

				if(city[i][j] < 0){

					city[i][j] = -city[i][j];
					if(j < city[0].length - 1){

						city[i][j + 1] = -city[i][j + 1];

					}

				}

			}

		}

	}

	public static void main(String args[]){

		int[][] city = buildCity();
		display(city);
    System.out.println("Invasion: ");
		int k = (int)(Math.random() * 20 + 30);
		invade(city, k);
		display(city);
    
    //to run update for 5 times
		for(int i = 0; i < 5; i++){
      
      System.out.println("Update: ");
			update(city);
			display(city);

		}

	}

}