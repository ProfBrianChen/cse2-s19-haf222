//Haoluo Fu
//02/08/2019
//This program is for determining how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;

public class Check{
  
  public static void main (String args[]){
    
    //get the user inputs 
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter the original cost of check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip you wish to pay as a whole number in the form xx.xx: ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;//We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //start calculating the cost for each person
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost/numPeople;//get the whole amount, dropping decimal fraction
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    //print out the cost for each person
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
  
}