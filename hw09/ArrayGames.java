//Haoluo Fu
//04/16/2019
//array games

//import scanner
import java.util.Scanner;
public class ArrayGames{
  
  //the method for generating array
  public static int[] generate(){
    
    int[] aven = new int [(int)(Math.random() * 11 + 10)];
    for(int z = 0; z < aven.length; z++){
      
      aven[z] = (int)(Math.random() * 49 + 1);
      
    }
    return aven;
    
  }
  
  //the method for printing array
  public static void print(int[] well){
    
    for(int i = 0; i < well.length; i++){
      
      System.out.print(well[i]+","+" ");
      
    }
    System.out.println("");
    
  }
  
  //the method for insert
  public static int[] insert(int[] we, int[] they){
    
    int[] result = new int [(int)(we.length + they.length)];
    int num = (int)(we.length * Math.random());
    
    //get the first part of the final array
    for(int i = 0; i < num; i++){
      
      result[i] = we[i];
      
    }
    
    //get the second part of the final array
    for(int i = 0; i < they.length; i++){
      
      result[i + num] = they[i];
      
    }
    
    //get the third part of the final array
    for(int i = num; i < we.length; i++){
      
      result[i + they.length] = we[i];
      
    };
    return result;
    
  }
  
  public static int[] shorten(int[] yap, int ab){
    
    if(yap.length > ab){
      
      int[] stop = new int [ab];
      
      //make a transitional array
      for(int z = 0; z < ab; z++){
        
        stop[z] = yap[z];
        
      }
      yap = stop;
      
    }
    return yap;
    
  }
  
  public static void main(String args[]){
    
    System.out.print("Do you want to run insert or shorten program? Answer 1 to insert or 2 to shorten   ");
    Scanner scan;
    scan = new Scanner(System.in);
    int answer = scan.nextInt();
    int[] array1 = generate();
    int[] array2 = generate();
    int[] fin;
    int j;
    System.out.print("Input 1: ");
    print(array1);

    
    if(answer == 1){
      
      System.out.print("Input 2: ");
      print(array2);      
      fin = insert(array1, array2);
      System.out.print("Output: ");
      print(fin);
      
    }
    
    if(answer == 2){
      
      System.out.print("Enter the number you want to shorten ");
      j = scan.nextInt();
      fin = shorten(array1, j);
      System.out.print("Output: ");
      print(fin);
    }
  }
  
}